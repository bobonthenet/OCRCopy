import subprocess, os
import mouse, keyboard
import time
import pytesseract
from PIL import ImageGrab, Image
import pyperclip
import platform

if "Windows-10" in platform.platform():
    keyboard.press_and_release('win+shift+s')
else:
    os.system('start SnippingTool.exe')

mouse.wait(button='left', target_types=('down'))
mouse.wait(button='left', target_types=('up'))
time.sleep(1)

if "Windows-7" in platform.platform():
    keyboard.press_and_release('ctrl+c')
    keyboard.press('alt')
    keyboard.press_and_release('f')
    keyboard.press_and_release('x')
    keyboard.release('alt')

im = ImageGrab.grabclipboard()
im.save('TextImage.png','PNG')

text = pytesseract.image_to_string(Image.open('TextImage.png'))
pyperclip.copy(text)
