# OCR Copy

This script will open the windows snipping tool, then after a screen shot has been taken text inside the image will be read and copied to your clipboard. From there you can paste it with ctr-v.

## Requirements
In order for this script to work you must have Python 3.x installed. You must also install all dependencies from the requirements.txt file. Lastly, you need to have [Google Tesseract OCR](https://github.com/tesseract-ocr/tesseract) installed. You must be able to invoke the tesseract command as tesseract. If this isn’t the case, for example because tesseract isn’t in your PATH and you will need to add it.

TODO: Include more detailed setup and install instructions.

## Running OCRCopy.py
Once you have all the requirements installed then you just need to run the command "python OCRCopy.py". The best way to use this script is to assign it to a hot key.
